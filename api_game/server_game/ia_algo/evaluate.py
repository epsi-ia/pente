from . import utilitaires

'''
Ce fichier contient les deux fonctions d'évaluation :
    -Pour la pente
    -Pour la tenaille

'''


'''
Fonction d'évaluation portant sur les intersections en
horizontale, verticale, et diagonales avec une visibilité
sur 4 intersections autour de celle évaluée (une pente étant
de cinq pions posés).
L'influence de l'intersection est augmentée de 10 pour
chaque pion rencontré.
Cette fonction est appellée par eval_pente.

@params
i,j    -> Int      -> Les coordonnées de l'intersection à évaluer
game_state   -> List[][] -> Le tableau du jeu à un état donné
player -> Int      -> Le joueur pour lequel on évalue

@return
score 	-> Int 		-> Le score de la case
'''
def eval_pente_axis(i, j, player, game_state):
	score = 0

	# On check 4 cases autour du pion posé en i,j
	for x in range(-4, 5):
		# vertical
		if i+x >= 0 and i+x <= 18:
			# gérer le cas ou on est sur notre case
			if(game_state[i+x][j] == player):
				score += 10

		# horizontal
		if j+x >= 0 and j+x <= 18:
			# gérer le cas de notre case
			if(game_state[i][j+x] == player):
				score += 10

		# diagonale droite
		if (i+x >= 0 and i+x <= 18) and (j+x >= 0 and j+x <= 18):
			if(game_state[i+x][j+x] == player):
				score += 10

		# diagonale gauche
		if (i+x >= 0 and i+x <= 18) and (j-x >= 0 and j-x <= 18):
			if(game_state[i+x][j-x] == player):
				score += 10

	return score


'''
Fonction permettant d'évaluer l'intersection la plus
influente du plateau (score maximum).

@params
player       -> Int      -> Le joueur pour lequel on évalue
game_state   -> List[][] -> Le tableau du jeu à un état donné
queue        -> Queue    -> La queue de résultat pour le threading

@return
score 	-> Int 	   -> Le meilleur score
'''
def eval_pente(player, game_state, i, j):
	return eval_pente_axis(i, j, player, game_state)

'''
Renvoie le score d'une case données
@params
j       -> Int 		-> Le joueur qui va placer un pion (1 ou 2)
tableau -> List[][] -> Le tableau d jeu à un état données
queue   -> Queue    -> La queue de résultat pour le threading

@return
score 	-> Int 		-> Le score de la case
'''

LIMITE_MIN 		= 2
LIMITE_MAX 		= 16
SCORE_TENAILLE  = 50

def evaluateTenaille(joueur, game_state, i, j):
	joueurActif  = joueur
	joueurPassif = 2 if (joueur == 1) else 1

	tenaille  		= str(joueurActif) + str(joueurPassif) + str(joueurPassif) + str(joueurActif)
	best_tenaille   = str(joueurActif) + str(joueurPassif) + str(joueurPassif) + str(joueurActif) + str(joueurPassif) + str(joueurPassif) + str(joueurActif)

	score_max	= -10000
	score 		= 0

	array_h		= ''
	array_v		= ''
	array_d_no	= ''
	array_d_ne	= ''

	for x in range(-3, 4):
		# vertical
		array_tmp = ''
		if i+x >= 0 and i+x <= 18:
			array_v += str(game_state[i+x][j])

		# horizontal
		if j+x >= 0 and j+x <= 18:
			array_h += str(game_state[i][j+x])

		# diagonale droite
		if (i+x >= 0 and i+x <= 18) and (j+x >= 0 and j+x <= 18):
			array_d_no += str(game_state[i+x][j+x])

		# diagonale gauche
		if (i+x >= 0 and i+x <= 18) and (j-x >= 0 and j-x <= 18):
			array_d_ne += str(game_state[i+x][j-x])

	array = [array_h, array_v, array_d_ne, array_d_no]

	for a in array:
		if(tenaille in a):
			score += SCORE_TENAILLE
		elif(best_tenaille in a):
			score += SCORE_TENAILLE*2

	if(score > score_max):
		score_max = score

	return score_max
