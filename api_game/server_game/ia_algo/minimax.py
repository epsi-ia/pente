import threading
from queue import Queue
from . import utilitaires, evaluate

PROFONDEUR_MAX = 2

'''
Choix de la stratégie à utiliser en min_max.
Sélection du score le plus fort.

@params
player       -> Int      -> Le joueur pour lequel on évalue
game_state   -> List[][] -> Le tableau du jeu à un état donné
i, j         -> Int, Int -> Les coordonnées du pion a simuler

@return
score 	-> Int 	   -> La somme des scores tenaille et pente
'''
def get_strategy_score(player, game_state, i, j):
	tenaille = evaluate.evaluateTenaille(player, game_state, i, j)
	pente = evaluate.eval_pente(player, game_state, i, j)

	# on retourne la case sur laquelle on a le plus de chance
	# de marquer des points que ça soit une pente ou une tenaille.
	return tenaille + pente

'''
Fonction de maximisation du gain de
l'algorithme min_max. Retourne le score maximum.

@params
joueur       -> Int      -> Le joueur pour lequel on évalue
game_state   -> List[][] -> Le tableau du jeu à un état donné
i, j         -> Int, Int -> Les corrdonnées du pion a simuler
profondeur   -> Int      -> La profondeur de l'arbre min_max (coups d'avance simulés)

@return
max_score 	-> Int 	   -> Le score maximum
'''
def max_play(joueur, game_state, i, j, profondeur):
	if(profondeur == PROFONDEUR_MAX):
		return get_strategy_score(joueur, game_state, i, j)

	max_score = 0
	for y in range(19):
		for x in range(19):
			if(game_state[x][y] == 0):
				joueurAdverse = 1 if joueur == 2 else 2
				score = min_play(joueurAdverse, game_state, x, y, profondeur+1)

				if score > max_score:
				  max_score = score

	return max_score


'''
Fonction de minimisation de la perte de
l'algorithme min_max. Retourne le score minimum.

@params
joueur       -> Int      -> Le joueur pour lequel on évalue
game_state   -> List[][] -> Le tableau du jeu à un état donné
i, j         -> Int, Int -> Les coordonnées du pion a simuler
profondeur   -> Int      -> La profondeur de l'arbre min_max (coups d'avance simulés)

@return
min_score 	-> Int 	   -> Le score minimum
'''
def min_play(joueur, game_state, i, j, profondeur):
	if(profondeur == PROFONDEUR_MAX):
		return get_strategy_score(joueur, game_state, i, j)

	min_score = 10000

	for y in range(19):
		for x in range(19):
			if(game_state[x][y] == 0):
				joueurAdverse = 1 if joueur == 2 else 2
				score = max_play(joueurAdverse, game_state, x, y, profondeur+1)

				if(score < min_score):
					min_score = score

	return min_score


'''
Algorithme min_max permettant de déduire le meilleur
mouvement à appliquer en fonction de la minimisation de la perte
(voir min_play) et de la maximisation du gain (voir max_play).

@params
game_state  -> List[][]            -> Le tableau du jeu à un état donné

@return
best_move 	-> dict(Int, Int) 	   -> Le meilleur mouvement aux coordonnées x,y
'''
def minimax(game_state, joueur):
	profondeur = 0

	q = Queue()
	move_score = []

	t1 = threading.Thread(target=minimax_thread, args=(0, 7, game_state, profondeur, joueur, q))
	t2 = threading.Thread(target=minimax_thread, args=(6, 13, game_state, profondeur, joueur, q))
	t3 = threading.Thread(target=minimax_thread, args=(12, 19, game_state, profondeur, joueur, q))

	threads = [t1, t2, t3]

	for t in threads:
		t.start()
		move_score.append(q.get())

	for t in threads:
		t.join()

	best = move_score[0]
	for item in move_score[1:]:
		if(item["best_score"] > best["best_score"]):
			best = item

	return best["best_move"]


def minimax_thread(start, stop, game_state, depth, player, queue):
	best_move = {}
	best_score = -10000

	for y in range(start, stop):
		for x in range(start, stop):
			if(game_state[x][y] == 0):
				game_state[x][y] = player
				score = min_play(player, game_state, x, y, depth+1)
				game_state[x][y] = 0

				if(score > best_score):
					best_move = {"x" : x, "y" : y}
					best_score = score

	queue.put({'best_move': best_move, 'best_score': best_score})
