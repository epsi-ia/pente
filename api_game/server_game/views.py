import random
from time import sleep

from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from server_game.ia_algo import game_init


class PlayView(APIView):
    def put(self, request, *args, **kwargs):
        data = request.data

        board = data["board"]
        score = data["score"]
        score_vs = data["score_vs"]
        player = data["player"]
        round = data["round"]

        # ici on a le code l'IA qui trouve les coo
        best_move = game_init.get_best_move(board, player)

        # x =
        #
        #
        # .randint(0,18)
        # y = random.randint(0,18)
        # best_move = {'x':x, 'y':y}
        # sleep(15)
        return Response(best_move, status=status.HTTP_200_OK, content_type='application/json')
