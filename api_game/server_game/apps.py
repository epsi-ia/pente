from django.apps import AppConfig


class ServerGameConfig(AppConfig):
    name = 'server_game'
